﻿//using MySql.Data.MySqlClient;
using MySqlConnector;
using Performance.Quartz.Hangfire.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Quartz
{
    public class SimpleJob : IJob
    {
        static SemaphoreSlim _semSlim;
        public SimpleJob()
        {
            _semSlim = new SemaphoreSlim(1);
        }

        public async Task Execute(IJobExecutionContext context)
        {
            //JobDataMap dataMap = context.JobDetail.JobDataMap;
            JobDataMap dataMap = context.MergedJobDataMap;
            //string parameterValue = dataMap.GetString("parameterTrigger");

            string conn = dataMap.GetString("conn");
            MySqlConnection connex = null;

            //string password = dataMap.GetString("password");
            //User user = (User)dataMap.Get("user");

            try
            {
                connex = new MySqlConnection(conn);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error NEW:" + ex.Message);
            }
            

            using (connex)
            {

                try
                {
                    if (connex.State == System.Data.ConnectionState.Closed)
                        connex.Open();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error CONN:" + ex.Message);
                }
                

                await _semSlim.WaitAsync();
                try
                {
                    var command = new MySqlCommand("INSERT INTO quartz_test.test (nombre, fechaHora) values('Prueba Quartz', SYSDATE());", connex);
                    await command.ExecuteNonQueryAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Error:" + ex.Message);
                }
                finally
                {
                    _semSlim.Release();
                }
            }

            //Debug.WriteLine($"SimpleJob is executed. The parameters are: {user.Username}, {user.Password} " +
            //    $"and parameter value is: {parameterValue}");
        }
    }
}
