﻿using Performance.Quartz.Hangfire.Models;
using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Quartz
{
    public class QuartzDemo : IQuartzDemo
    {
        IScheduler _scheduler;
        public QuartzDemo(IScheduler scheduler)
        {
            _scheduler = scheduler;
        }
        public async Task TestQuartz(string conn)
        {
            IJobDetail job = JobBuilder.Create<SimpleJob>()
                .UsingJobData("conn", conn)
                //.UsingJobData("password", "12345")
                //.WithIdentity("SimpleJob", "QuartzExample1")
                .StoreDurably() // para que no lo elimine y pueda reutilizarse
                .Build();

            //job.JobDataMap.Put("user", new User { Username = "Sebastian", Password = "1234" });

            //if (await _scheduler.CheckExists(job.Key))
            //{
            //    await _scheduler.DeleteJob(job.Key);
            //}

            await _scheduler.AddJob(job, true);


            ITrigger trigger = TriggerBuilder.Create()
                .ForJob(job) //para usar en multi trigger
                //.UsingJobData("parameterTrigger", "this is the value of the parameter the Trigger1")
                //.WithIdentity("testTrigger", "QuartzExample1")
                .StartNow()
                //.WithSimpleSchedule(x => x.WithIntervalInSeconds(5).WithRepeatCount(8))
                .Build();

            //ITrigger trigger2 = TriggerBuilder.Create()
            //    .ForJob(job) //para usar en multi trigger
            //    .UsingJobData("parameterTrigger", "this is the value of the parameter the Trigger2")
            //    .WithIdentity("testTrigger2", "QuartzExample1")
            //    .StartNow()
            //    .WithSimpleSchedule(x => x.WithIntervalInSeconds(5).WithRepeatCount(8))
            //    .Build();


            await _scheduler.ScheduleJob(trigger);
            //await _scheduler.ScheduleJob(trigger2);

        }
    }
}
