﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Quartz
{
    public interface IQuartzDemo
    {
        Task TestQuartz(string conn);
    }
}
