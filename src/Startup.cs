using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;
using Hangfire;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Performance.Quartz.Hangfire.Extensions;
using Performance.Quartz.Hangfire.Hangfire;
using Performance.Quartz.Hangfire.Quartz;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CrystalQuartz.Owin;
using CrystalQuartz.AspNetCore;
using MySqlConnector;
using Quartzmin;

namespace Performance.Quartz.Hangfire
{
    public class Startup
    {
        IScheduler _scheduler;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();

            //HANGFIRE
            services.ConfigureHangFire(Configuration);

            //QUARTZ
            _scheduler = services.ConfigureQuartz(Configuration);

            services.AddSingleton<IHangfireDemo, HangfireDemo>();
            services.AddSingleton<IQuartzDemo, QuartzDemo>();
            services.AddSingleton<IBackgroundJobs, BackgroundJobsImplementation>();

            //services.AddQuartzmin();
            //services.AddTransient<MySqlConnection>(_ => new MySqlConnection(Configuration["mysqlconnection:connHangfireMySql"]));

            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Performance.Quartz.Hangfire", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Performance.Quartz.Hangfire v1"));
            }

            //_scheduler.JobFactory = new AspnetCoreJobFactory(app.ApplicationServices);
            //_scheduler.Start().Wait();

            app.UseRouting();
            app.UseAuthorization();

            //HANGFIRE
            app.UseHangfireDashboard();

            //app.UseQuartzmin(new QuartzminOptions()
            //{
            //    Scheduler = StdSchedulerFactory.GetDefaultScheduler().Result
            //});

            app.UseCrystalQuartz(()=> _scheduler);

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                //endpoints.MapHangfireDashboard();
            });
        }
    }
}
