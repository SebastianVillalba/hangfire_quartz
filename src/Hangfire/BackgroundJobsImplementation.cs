﻿//using MySqlConnector;
//using MySql.Data.MySqlClient;
using MySqlConnector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Hangfire
{
    public class BackgroundJobsImplementation : IBackgroundJobs
    {
        public Task MethodToRun(string conn)
        {
            MySqlConnection connex = new MySqlConnection(conn);

            using (connex)
            {
                if (connex.State == System.Data.ConnectionState.Closed)
                    connex.Open();

                var command = new MySqlCommand("INSERT INTO test_hangfire.test (nombre, fechaHora) values('Prueba HangFire', SYSDATE());", connex);
                return command.ExecuteNonQueryAsync();
            }
        }
    }
}
