﻿using Hangfire;
using MySqlConnector;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Hangfire
{
    public class HangfireDemo: IHangfireDemo
    {
        public Task TestHangfire(string conn) 
        {
            return Task.Run(() =>  BackgroundJob.Enqueue<IBackgroundJobs>(x => x.MethodToRun(conn)));
        }
    }
}
