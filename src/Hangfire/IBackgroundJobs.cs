﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Hangfire
{
    public interface IBackgroundJobs
    {
        Task MethodToRun(string conn);
    }
}
