﻿using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Hangfire
{
    public interface IHangfireDemo
    {
        Task TestHangfire(string conn);
        //Task MethodToRun(string conn);
    }
}
