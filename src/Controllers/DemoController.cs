﻿using BenchmarkDotNet.Reports;
using BenchmarkDotNet.Running;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySqlConnector;
using Performance.Quartz.Hangfire.Hangfire;
using Performance.Quartz.Hangfire.Quartz;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Performance.Quartz.Hangfire.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class DemoController : Controller
    {
        IHangfireDemo _hangfireDemo;
        IQuartzDemo _quartzDemo;
        IConfiguration _configuration;

        public DemoController(IHangfireDemo hangFireDemo, IQuartzDemo quartzDemo, IConfiguration configuration )
        {
            _hangfireDemo = hangFireDemo;
            _quartzDemo = quartzDemo;
            _configuration = configuration;
        }

        [HttpGet("Get")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async void Get()
        {
            List<Task> tasksList = new List<Task>();
            Stopwatch clock = new Stopwatch();

            clock.Start();
            //for (int i = 0; i < 4000; i++)
            //    await _hangfireDemo.TestHangfire(_configuration["mysqlconnection:connHangfireMySql"]);

            for (int i = 0; i < 4000; i++)
            {
                await _quartzDemo.TestQuartz(_configuration["mysqlconnection:connQuartzMySql"]);
            }
                
            //tasksList.Add(_quartzDemo.TestQuartz(_configuration["mysqlconnection:connQuartzMySql"]));

            //await Task.WhenAll(tasksList);
            clock.Stop();
            //conn.Clone();

            Console.WriteLine("Time: " + String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
            clock.Elapsed.Hours, clock.Elapsed.Minutes, clock.Elapsed.Seconds,
            clock.Elapsed.Milliseconds / 10));
        }
    }
}
