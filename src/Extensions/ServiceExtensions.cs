﻿using Hangfire;
using Hangfire.MySql;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;

namespace Performance.Quartz.Hangfire.Extensions
{
    public static class ServiceExtensions
    {
        public static void ConfigureHangFire(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHangfire(config =>
            config.UseStorage(new MySqlStorage(
                            configuration["mysqlconnection:connHangfireMySql"],
                            new MySqlStorageOptions
                            {
                                TransactionIsolationLevel = IsolationLevel.ReadCommitted, // Transaction isolation level. The default is to read submitted.
                                QueuePollInterval = TimeSpan.FromSeconds(15), //- The job queue polling interval. The default value is 15 seconds.
                                JobExpirationCheckInterval = TimeSpan.FromHours(1), //- Job expiration check interval (manage expired records). The default value is 1 hour.
                                CountersAggregateInterval = TimeSpan.FromMinutes(5), //- Aggregate counter interval. The default is 5 minutes.
                                PrepareSchemaIfNecessary = true, //- If set to true, the database table is created. The default is true.
                                DashboardJobListLimit = 50000, //- Dashboard job list limit. The default value is 50000.
                                TransactionTimeout = TimeSpan.FromMinutes(1)//, //- Transaction timeout. The default is 1 minute.   
                                //TablesPrefix = "Hangfire" //- The prefix of the table in the database. Default is none
                            }))
            .SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                    .UseSimpleAssemblyNameTypeSerializer()
                    .UseDefaultTypeSerializer());

            services.AddHangfireServer();
        }

        //public static IScheduler ConfigureQuartz(IConfiguration configuration)
        //{
        //    NameValueCollection props = new NameValueCollection
        //    {
        //        {"quartz.serializer.type","json" },
        //        {"quartz.jobStore.type","Quartz.Impl.AdoJobStore.JobStoreTX, Quartz" },
        //        {"quartz.jobStore.dataSource","default" },
        //        {"quartz.dataSource.default.provider","MySql" },
        //        {"quartz.dataSource.default.connectionString", configuration["mysqlconnection:connQuartzMySql"] }
        //    };

        //    return new StdSchedulerFactory(props).GetScheduler().Result;
        //    //scheduler.Start().Wait();

        //    //services.AddSingleton(Provider => scheduler);
        //}

        public static IScheduler ConfigureQuartz(this IServiceCollection services, IConfiguration configuration)
        {
            NameValueCollection props = new NameValueCollection
            {
                {"quartz.serializer.type","json" },
                {"quartz.jobStore.type","Quartz.Impl.AdoJobStore.JobStoreTX, Quartz" },
                {"quartz.jobStore.dataSource","default" },
                {"quartz.dataSource.default.provider","MySql" },
                {"quartz.dataSource.default.connectionString", configuration["mysqlconnection:connQuartzMySql"] }
            };

            IScheduler sch = new StdSchedulerFactory(props).GetScheduler().Result;
            sch.Start().Wait();

            services.AddSingleton(Provider => sch);

            return sch;
        }
    }
}
